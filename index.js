const db = require('./db.js');
const app = require('./app.js');

const PORT = 5001;

db.connect()
  .then(() => {
    app.listen(PORT, () => {
      console.log('Listening on port: ' + PORT);
    });
  });


 


