const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const db = require('./db.js');

const Contact = require('./contact'); // on importe notre model

app.use(bodyParser()); 


app.post('/', async (req, res) => {
    const nom = req.body.nom; // récupération des variables du body
    const prenom = req.body.prenom;
    const email  = req.body.email;
    const phone  = req.body.phone;
    const adresse = req.body.adresse;
    const idContact = req.body.idContact;
 
    if (!nom) { // on vérifie que le nom est présent
        res.send('Il manque un argument')
        return
    }
 
    const nouveau_contact = new Contact({ // création d'un objet représentant notre nouveau contact
        idContact : idContact,
        nom : nom,
        prenom : prenom,
        adresse : adresse,
        email : email,
        phone : phone
        })
     
    await nouveau_contact.save() // sauvegarde asynchrone du nouveau contact
    res.json(nouveau_contact)
    return
 
});

app.get('/', async (req, res) => {
    const contacts = await Contact.find() // On récupère tous les contacts
    await res.json(contacts);
});


app.get('/:id', async (req, res) => {
    const id = req.params.id // on récupère la valeure dans l'url
    const contact = await Contact.findOne({_id : id}) // on récupère le contact grâce à son _id
    res.json(contact);
    return contact.nom;
});

app.get('/:nom', async (req, res) => {
    const nom = req.params.nom // on récupère la valeure dans l'url
    const contact = await Contact.findOne({nom : nom}) // on récupère le contact grâce à son _id
    res.json(contact);
    return contact;
});

app.delete('/:id', async(req, res) => {
    const id = req.params.id
    const suppr = await Contact.deleteOne({_id : id})
    res.json(suppr)
     
});

app.delete('/:nom', async(req, res) => {
    const nom = req.params.nom
    const suppr = await Contact.deleteOne({nom : nom})
    res.json(suppr) 
     
});

app.delete('/', async(req, res) => {
    const suppr = await Contact.deleteMany();
    res.json(suppr)
     
});

app.patch('/:idContact', async(req, res) => {
    // on récupère les valeurs potentiellement modifiées
    const idContact = req.params.idContact;
    const nom = req.body.nom;
    const prenom = req.body.prenom;
    const email  = req.body.email;
    const phone  = req.body.phone;
    const adresse = req.body.adresse;
      
    let filtre = { idContact: idContact};
    let update = { nom: nom, prenom: prenom, email: email, phone: phone, adresse: adresse};
    
    const contact = await Contact.findOneAndUpdate(filtre, update) // on récupere le contact pour pouvoir le modifier et le sauvegarde
    
    res.json(contact)
     
})





module.exports = app;
