const expect = require('chai').expect;
const request = require('supertest');

const app = require('../app.js');
const conn = require('../db.js');


// test POST

describe('POST /', () => {
    before((done) => {
        conn.connect()
            .then(() => done())
            .catch((err) => done(err));
    })

    after((done) => {
        conn.close()
            .then(() => done())
            .catch((err) => done(err));
    })

    it('OK, creating a new contact works', (done) => {
        request(app).post('/')
            .send(
                {
                    nom: 'nom de test',
                    prenom: "prenom de test",
                    email: "email@test.com",
                    phone: "0033611111111",
                    adresse: "adresse de test"
                })
            .then((res) => {
                const body = res.body;
                expect(body).to.contain.property('_id');
                expect(body).to.contain.property('nom');
                expect(body).to.contain.property('prenom');
                expect(body).to.contain.property('email');
                expect(body).to.contain.property('phone');
                expect(body).to.contain.property('adresse');
                done();
            })
            .catch((err) => done(err));
    });

    it('Fail, created contact requires name', (done) => {
        request(app).post('/')
            .send(
                {
                    prenom: "prenom de test",
                    email: "email@test.com",
                    phone: "0033611111111",
                    adresse: "adresse de test"
                })
            .then((res) => {
                const body = res.body;
                console.log(body);
                done();
            })
            .catch((err) => done(err));
    });
})


// test GET
// La BD n'est pas empty car un contact vient d'être inséré dans le test POST

describe('GET /', () => {
    before((done) => {
        conn.connect()
            .then(() => done())
            .catch((err) => done(err));
    })

    after((done) => {
        conn.close()
            .then(() => done())
            .catch((err) => done(err));
    })

    it('OK, DataBase is not empty', (done) => {
        request(app).get('/')
            .then((res) => {
                const body = res.body;
                console.log(body);
                expect(body.length).to.be.not.equal(0);
                done();
            })
            .catch((err) => done(err));
    })
})




// test DELETE

describe('DELETE /', () => {
    before((done) => {
        conn.connect()
            .then(() => done())
            .catch((err) => done(err));
    })

    after((done) => {
        conn.close()
            .then(() => done())
            .catch((err) => done(err));
    })

    it('OK, contacts were deleted', (done) => {
        request(app).delete('/')
            .then((res) => {
                const body = res.body;
                console.log(body);
                done();
            })
            .catch((err) => done(err));
    });
})


// test PATCH


describe('PATCH /:id', () => {
    before((done) => {
        conn.connect()
            .then(() => done())
            .catch((err) => done(err));
    })

    after((done) => {
        conn.close()
            .then(() => done())
            .catch((err) => done(err));
    })

    it('OK, updating contact', (done) => {
        request(app).patch('/1')
            .send(
                {
                    nom: 'Oussema',
                    prenom: "nouveau prenom",
                    email: "email@test.com",
                    phone: "0033611111111",
                    adresse: "adresse de test"
                })
            .then((res) => {
                const body = res.body;
                done();
            })
            .catch((err) => done(err));
    });

})


