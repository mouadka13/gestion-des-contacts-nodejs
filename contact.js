const mongoose = require('mongoose');

const schema = mongoose.Schema({
    idContact : Number,
    nom : 
        { 
        type : String,
        required : true
        },
    prenom : String,
    adresse : String,
    email : String,
    phone : String
})

module.exports = mongoose.model('contact', schema);