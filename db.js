const mongoose = require('mongoose');
const DB_URI = 'mongodb://127.0.0.1:27017/db';

function connect() {
  return new Promise((resolve, reject) => {
        mongoose.connect(DB_URI,
          { useNewUrlParser: true })
          .then((res, err) => {
            if (err) return reject(err);
            resolve();
          })
  });
}

function close() {
  return mongoose.disconnect();
}

module.exports = { connect, close };
